package com.example.design.creation.abstractfactory;

/**
 * 不关心实现细节，但是使用方知道要使用的工厂的产品功能。
 *
 * @author Administrator
 */
public class IndianMain {
    public static void main(String[] args) {

        // 买中国工厂的J20
        BigDaddyBidAbstractFactory factory = new ChinaBidFactory();
        factory.getFly().fly();

        // 买美国工厂的  SCAR
        factory = new USBidFactory();
        factory.getGun().pong();
    }
}

// ================
interface Gun {
    void pong();
}

// ================
class Gun97 implements Gun {
    @Override
    public void pong() {
        System.out.println("97 步枪出击");
    }
}

// ================
class GunScar implements Gun {
    @Override
    public void pong() {
        System.out.println("SCAR 步枪出击");
    }
}

// ================
interface Plan {
    void fly();
}

// ================
class PlanJ20 implements Plan {

    @Override
    public void fly() {
        System.out.println("J20 飞飞飞");
    }
}

// ================
class PlanF14 implements Plan {

    @Override
    public void fly() {
        System.out.println("F14 飞飞飞");
    }
}

// ================
interface BigDaddyBidAbstractFactory {

    Gun getGun();

    Plan getFly();
}

// ================
class ChinaBidFactory implements BigDaddyBidAbstractFactory {

    @Override
    public Gun getGun() {
        return new Gun97();
    }

    @Override
    public Plan getFly() {
        return new PlanJ20();
    }
}

// ================
class USBidFactory implements BigDaddyBidAbstractFactory {

    @Override
    public Gun getGun() {
        return new GunScar();
    }

    @Override
    public Plan getFly() {
        return new PlanF14();
    }
}

