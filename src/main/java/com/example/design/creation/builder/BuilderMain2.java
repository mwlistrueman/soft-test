package com.example.design.creation.builder;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * okhttp 中的builder 示例模仿
 *
 * @author Administrator
 */
public class BuilderMain2 {
    public static void main(String[] args) {
        Order order = new OrderBuilder(Arrays.asList("麦辣鸡腿堡", "那么大杯快乐水", "缤纷夏日彩虹桥")).build();
        System.out.println("先生|女士您的订单如下:");
        System.out.println(order);
/*
    先生|女士您的订单如下:
    Order{orderId='1027940756', offerList=[麦辣鸡腿堡, 那么大杯快乐水, 缤纷夏日彩虹桥], orderPrice='100RMB', shopAddress='商店名称', orderTime=2022-08-20T23:29:38.281}
*/
    }
}


class OrderBuilder {

    private final List<String> offerList;

    /**
     * 构造订单时，只需要传入点的 销售品名称即可。可用于强制限定哪些参数在构造时是必须要传入的
     *
     * @param offerList
     */
    public OrderBuilder(List<String> offerList) {
        this.offerList = offerList;
    }

    /**
     * 这里可以做很多事情
     *
     * @return
     */
    public Order build() {
        Integer orderId = Math.abs(new Random().nextInt());
        String orderPrice = "100RMB"; // 这里默认给个价格。实际应该是综合销售品信息计算订单价格。
        String shopAddress = "商店名称"; // 这里默认给个名称。商店名称 应该是根据登录账号的位置去数据库获取
        LocalDateTime orderTime = LocalDateTime.now();
        return new Order(orderId, offerList, orderPrice, shopAddress, orderTime);
    }

}

class Order {
    private final Integer orderId;
    /**
     * 简化描述的点餐信息。复杂一些的话，这里String 换成Object
     */
    private final List<String> offerList;

    private final String orderPrice;

    private final String shopAddress;
    private final LocalDateTime orderTime;

    public Order(Integer orderId, List<String> offerList, String orderPrice, String shopAddress, LocalDateTime orderTime) {
        this.orderId = orderId;
        this.offerList = offerList;
        this.orderPrice = orderPrice;
        this.shopAddress = shopAddress;
        this.orderTime = orderTime;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId='" + orderId + '\'' +
                ", offerList=" + offerList +
                ", orderPrice='" + orderPrice + '\'' +
                ", shopAddress='" + shopAddress + '\'' +
                ", orderTime=" + orderTime +
                '}';
    }
}

