package com.example.design.creation.builder;

/**
 * 经典软考书中的 Builder：
 * 1. 需要一个Builder 为创建 Product 对象的各个步骤指定接口。
 * 2. Director 来组织 builder 的构造关系提供一个 construct 方法，来获取 Builder 创建好的 Product 对象。
 *
 * @author Administrator
 */
public class BuilderMain1 {
    public static void main(String[] args) {
        Main1Builder builder = new Concreate1Builder();
        Director director = new Director(builder);
        Main1Product product = director.construct();
        System.out.println(product.toString()); // Main1Product{partA='A1部分构建好了', partB='B1部分构建好了'}

        builder = new Concreate2Builder();
        director = new Director(builder);
        product = director.construct();
        System.out.println(product.toString()); // Main1Product{partA='A2部分构建好了', partB='B2部分构建好了'}
    }
}

class Main1Product {
    private String partA;
    private String partB;

    public String getPartA() {
        return partA;
    }

    public void setPartA(String partA) {
        this.partA = partA;
    }

    public String getPartB() {
        return partB;
    }

    public void setPartB(String partB) {
        this.partB = partB;
    }

    @Override
    public String toString() {
        return "Main1Product{" +
                "partA='" + partA + '\'' +
                ", partB='" + partB + '\'' +
                '}';
    }
}

abstract class Main1Builder {
    Main1Product product = new Main1Product();

    abstract void setPartA();

    abstract void setPartB();

    Main1Product getResult() {
        return product;
    }
}

class Concreate1Builder extends Main1Builder {
    @Override
    void setPartA() {
        product.setPartA("A1部分构建好了");
    }

    @Override
    void setPartB() {
        product.setPartB("B1部分构建好了");
    }
}

class Concreate2Builder extends Main1Builder {
    @Override
    void setPartA() {
        product.setPartA("A2部分构建好了");
    }

    @Override
    void setPartB() {
        product.setPartB("B2部分构建好了");
    }
}

class Director {
    private Main1Builder builder;

    public Director(Main1Builder builder) {
        this.builder = builder;
    }

    public Main1Product construct() {
        builder.setPartA();
        builder.setPartB();
        return builder.getResult();
    }
}



