package com.example.design.creation.prototype;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * @author Administrator
 */
public class DepthCloneMain {
    public static void main(String[] args) {
        Man man1 = new Man("张三", 12, "北京", Arrays.asList(new Hobby("篮球")));
        Man man2 = man1.depthCopy();
        man2.setName("李四");
        man2.setHobbyList(Arrays.asList(new Hobby("足球")));
        man1.show(); // Man{name='张三', age=12, address='北京', hobbyList=[Hobby{name='篮球'}]}
        man2.show(); // Man{name='李四', age=12, address='北京', hobbyList=[Hobby{name='足球'}]}
        System.out.println(man1); // example.design.prototype.Man@4d7e1886
        System.out.println(man2); // example.design.prototype.Man@30dae81
    }
}

class Man implements Serializable {
    private String name;
    private Integer age;
    private String address;

    private List<Hobby> hobbyList;

    public Man(String name, Integer age, String address, List<Hobby> hobbyList) {
        this.name = name;
        this.age = age;
        this.address = address;
        this.hobbyList = hobbyList;
    }

    public void setHobbyList(List<Hobby> hobbyList) {
        this.hobbyList = hobbyList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Man depthCopy() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream ous = new ObjectOutputStream(out);
            ous.writeObject(this);

            ByteArrayInputStream bais = new ByteArrayInputStream(out.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            Man mainCopy = (Man) ois.readObject();

            ois.close();
            bais.close();
            ous.close();
            out.close();
            return mainCopy;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void show() {
        System.out.println("Man{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", address='" + address + '\'' +
                ", hobbyList=" + hobbyList +
                '}');
    }
}

class Hobby implements Serializable{
    public String name;

    public Hobby(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "name='" + name + '\'' +
                '}';
    }
}




