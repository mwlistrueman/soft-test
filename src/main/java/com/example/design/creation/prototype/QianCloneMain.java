package com.example.design.creation.prototype;

/**
 * @author Administrator
 */
public class QianCloneMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person person1 = new Person("张三", 12, "北京海淀区");
        Person person2 = (Person) person1.clone();
        person2.setName("李四");
        System.out.println(person1); // Person{name='张三', age='12', address='北京海淀区'}
        System.out.println(person2); // Person{name='李四', age='12', address='北京海淀区'}
    }
}

class Person implements Cloneable {
    private String name;
    private Integer age;
    private String address;

    public Person(String name, Integer age, String address) {
        this.name = name;
        this.age = age;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public void setName(String name) {
        this.name = name;
    }
}


