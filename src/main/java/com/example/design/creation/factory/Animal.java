package com.example.design.creation.factory;

public interface Animal {

    void show();

}
