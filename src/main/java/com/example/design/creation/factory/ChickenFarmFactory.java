package com.example.design.creation.factory;

/**
 * @author Administrator
 */
public class ChickenFarmFactory implements FarmFactory {

    @Override
    public Animal newAnimal() {
        return new ChickenAnimal("");
    }
}
