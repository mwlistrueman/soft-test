package com.example.design.creation.factory;

public class FactoryMain {

    public static void main(String[] args) {

        FarmFactory factory = (FarmFactory)FactoryMain.getFactory(ChickenFarmFactory.class);
        factory.newAnimal().show();

        factory = (FarmFactory)FactoryMain.getFactory(HorseFarmFactory.class);
        factory.newAnimal().show();
    }

    /**
     * 根据 入参类型确定使用哪个实现类
     *
     * @param clazz
     * @return
     */
    public static Object getFactory(Class clazz){
        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
