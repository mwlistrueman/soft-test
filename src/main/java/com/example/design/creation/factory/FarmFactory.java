package com.example.design.creation.factory;

/**
 * @author Administrator
 */
public interface FarmFactory {

    Animal newAnimal();
}
