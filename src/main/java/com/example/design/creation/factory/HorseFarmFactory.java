package com.example.design.creation.factory;

/**
 * @author Administrator
 */
public class HorseFarmFactory implements FarmFactory {

    @Override
    public Animal newAnimal() {
        return new HorseAnimal();
    }
}
