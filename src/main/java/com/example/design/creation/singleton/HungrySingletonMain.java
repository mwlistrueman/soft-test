package com.example.design.creation.singleton;

public class HungrySingletonMain {
    public static void main(String[] args) {
        HungryInstance instance = HungryInstance.getInstance();
        instance.show();
    }
}

// ==================
class HungryInstance {

    private static HungryInstance hungryInstance = new HungryInstance();

    public static synchronized HungryInstance getInstance() {
        return hungryInstance;
    }

    public void show() {
        System.out.println("MyInstance#show()");
    }
}
