package com.example.design.creation.singleton;

/**
 * @author Administrator
 */
public class LazySingletonMain {

    public static void main(String[] args) {
        HungryInstance instance = HungryInstance.getInstance();
        instance.show();
    }
}

// ==================
class LazyInstance {

    private static HungryInstance hungryInstance;

    public static synchronized HungryInstance getInstance(){
        if (hungryInstance == null) {
            return hungryInstance = new HungryInstance();
        }
        return hungryInstance;
    }

    public void show(){
        System.out.println("MyInstance#show()");
    }
}
