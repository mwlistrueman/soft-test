package com.example.design.structure;


import java.util.HashMap;

/**
 * 场景：现在有一个人，有个背包，有一只笔刷，他需要创造一个泥人，笔刷和吹风机可以重复使用，但是做泥人的泥只能用一次。
 *
 * @author Administrator
 */
public class FlyWeightMain {
    public static void main(String[] args) {
        // 数据初始化
        BrushUtilFlyWight brushUtilFlyWight = new BrushUtilFlyWight();
        DirerUtilFlyWight direrUtilFlyWight = new DirerUtilFlyWight();
        PackageFlyWeightFactory.map.put("brush", brushUtilFlyWight);
        PackageFlyWeightFactory.map.put("direr", direrUtilFlyWight);

        // 捏泥人
        Mud mud1 = new Mud("唐僧");
        PackageFlyWeightFactory.map.get("brush").use(mud1);
        PackageFlyWeightFactory.map.get("direr").use(mud1);
        System.out.println("=========唐僧完成============");

        // 捏泥人
        Mud mud2 = new Mud("孙悟空");
        PackageFlyWeightFactory.map.get("brush").use(mud2);
        PackageFlyWeightFactory.map.get("direr").use(mud2);
        System.out.println("=========孙悟空完成============");
    }
}

// 接口，定义一个方法。
interface UtilFlyWight {
    void use(Mud mud);
}

class BrushUtilFlyWight implements UtilFlyWight {
    @Override
    public void use(Mud mud) {
        System.out.println("使用笔刷给泥人【" + mud.getName() + "】上色");
    }
}

class DirerUtilFlyWight implements UtilFlyWight {
    @Override
    public void use(Mud mud) {
        System.out.println("使用吹风机给泥人【" + mud.getName() + "】吹干");
    }
}

class Mud {
    private final String name;

    public Mud(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

// 模拟背包
class PackageFlyWeightFactory {
    public static HashMap<String, UtilFlyWight> map = new HashMap<>();

    public UtilFlyWight getFlyWeight(UtilFlyWight key) {
        if (map.get(key) == null) {
            return null; // 如果对象不存在，则返回个别的东西。例如：null
        }
        return map.get(key);
    }
}

