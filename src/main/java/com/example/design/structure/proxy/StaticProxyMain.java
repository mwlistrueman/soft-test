package com.example.design.structure.proxy;


/**
 * @author Administrator
 */
public class StaticProxyMain {
    public static void main(String[] args) {
        new FileProxy("txt").showList();
        new FileProxy("excel").showList();
    }
}

// ============= 被代理对象接口
interface File {

    //    一个接口展示所有的文件列表
    void showList();
}

// =========实现类1
class TxtFile implements File {
    @Override
    public void showList() {
        System.out.println("当前系统所有的Txt文件有： a.txt,b.txt");
    }
}

// =========实现类2
class ExcelFile implements File {
    @Override
    public void showList() {
        System.out.println("当前系统所有的Excel文件有：c.excel,d.excel");
    }
}



// ==========代理类 (实现File接口，因为要代理 File 的功能（个人认为，是否实现File接口依赖于是否和File功能名保持一致）
class FileProxy implements File {

    private final File file;

    public FileProxy(String txtType) {
        if (txtType.equals("txt")) {
            file = new TxtFile();
        } else {
            this.file = new ExcelFile();
        }
    }

    @Override
    public void showList() {
        System.out.println("调用开始，日志记录=====");
        file.showList();
        System.out.println("调用结束，日志记录=====");
    }
}

