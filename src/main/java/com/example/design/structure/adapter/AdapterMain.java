package com.example.design.structure.adapter;

import java.util.Arrays;

/**
 * 现在有一个新系统，使用Mysql开发，但是老系统使用SQL Server进行开发的，但是原来的SqlServer开发的获取数据接口不可修改，需要的数据和SQlserver获取到的也有一些不同。
 * 现在要写一个适配器类，供给开发人员调用，让开发无感知数据异常。就像调用Mysql的数据一样
 * 实现适配老系统的其中一个获取所有数据功能。
 *
 * @author Administrator
 */
public class AdapterMain {
    public static void main(String[] args) {

        // 指明要调用的对象，需要让适配器进行适配
        SqlServerAdaptee sqlServerAdaptee = new SqlServerAdaptee();
        String[] allInfo = new SqlServerAdapter(sqlServerAdaptee).getAllInfo();
        Arrays.stream(allInfo).forEach(System.out::println);
        /**
         * 数据1
         * 数据2
         * 数据3
         */
    }
}

// 被适配的对象。
class SqlServerAdaptee {
    public String getInfo(){
        return "数据1，数据2，数据3";
    }
}

// 一个适配器的接口类,规范适配器需要实现的功能
interface TargetAdapter{
    String [] getAllInfo();
}

class SqlServerAdapter implements TargetAdapter{
    private SqlServerAdaptee sqlServerAdaptee;

    public SqlServerAdapter(SqlServerAdaptee sqlServerAdaptee) {
        this.sqlServerAdaptee = sqlServerAdaptee;
    }


    @Override
    public String [] getAllInfo() {
        String sqlServerAdapteeInfo = sqlServerAdaptee.getInfo();
//        适配器对数据进行包装,调整
        return sqlServerAdapteeInfo.split("，");
    }
}

